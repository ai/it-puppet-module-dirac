#
# This docker file can be used to run tests for this hostgroup or
# module locally on a desktop or on aiadm assuming docker or podman is available and working
#
# First generate a gitlab token to auth against gitlab, visit:
#
# https://gitlab.cern.ch/-/profile/personal_access_tokens?name=Read+Access+token&scopes=read_repository
#
# To run all tests use:
# podman build --build-arg GITLAB_ACCESS_TOKEN=<your_token>  .
#
# To run a single individual spec file:
# podman build --build-arg GITLAB_ACCESS_TOKEN=<your_token> --build-arg SPEC=spec/defines/alias_spec.rb  .
#
#
FROM gitlab-registry.cern.ch/ai/it-puppet-module-ci_images:alma9-puppet-ci-2.10.1

WORKDIR /opt/puppet

ARG PARALLEL_TEST_PROCESSORS=2
ARG GITLAB_ACCESS_TOKEN=foobar
ARG RUBY_VERSION='2.7.8'
ARG PUPPET_VERSION='~> 7.0'
ARG FACTER_GEM_VERSION='~> 4.0'
ARG SPEC=''

COPY ci ci
# Cache gems
RUN mkdir code ; cd code ; BUNDLE_GEMFILE=../ci/Gemfile FACTER_GEM_VERSION=${FACTER_GEM_VERSION} PUPPET_VERSION=${PUPPET_VERSION} bundle install --local
# Cache fixtures
COPY code/.fixtures.yml code/.fixtures.yml
RUN cd code ; sed -i "s#ssh://git@gitlab.cern.ch:7999/#https://gitlab-ci-token:${GITLAB_ACCESS_TOKEN}@gitlab.cern.ch/#" .fixtures.yml
RUN cd code ; BUNDLE_GEMFILE=../ci/Gemfile FACTER_GEM_VERSION=${FACTER_GEM_VERSION} PUPPET_VERSION=${PUPPET_VERSION} bundle exec rake --rakefile ../ci/Rakefile spec_prep


# Use latest code, this will also check if any fixtures need updating
COPY . .
RUN cd code ;\
  sed -i "s#ssh://git@gitlab.cern.ch:7999/#https://gitlab-ci-token:${GITLAB_ACCESS_TOKEN}@gitlab.cern.ch/#" .fixtures.yml ;\
  eval "$(rbenv init - bash)" ;\
  rbenv global ${RUBY_VERSION} ;\
  if [ -n "$SPEC" ] ; then \
    BUNDLE_GEMFILE=../ci/Gemfile FACTER_GEM_VERSION=${FACTER_GEM_VERSION} PUPPET_VERSION=${PUPPET_VERSION} bundle exec rspec --fail-fast $SPEC ;\
  else \
    BUNDLE_GEMFILE=../ci/Gemfile FACTER_GEM_VERSION=${FACTER_GEM_VERSION} PUPPET_VERSION=${PUPPET_VERSION} bundle exec rake --rakefile ../ci/Rakefile release_checks ;\
  fi



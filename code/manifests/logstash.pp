# @param logstash_tpl_path parameter to generate the logstash.conf file
# @param logstash_server install a logstash server
# @param logstash_config_args dictionary that can be used in the lostash templates (e.g. <%= logstash_config_args['mb_brokers'] %>)
# @param logstash_settings settings to give logstash
# @param logstash_jvm_options jvm options
# @param teigi_keys list of keys to resolve

class dirac::logstash(
  String $logstash_tpl_path = 'dirac/logstash.conf.erb',
  Boolean $logstash_server = false,
  Hash $logstash_config_args = {},
  Hash $logstash_settings = { 'pipeline.ecs_compatibility' => 'disabled', },
  Array $logstash_jvm_options = [
              '-Xms1g',
              '-Xmx1g',
            ],
  Array $teigi_keys = [],
){
    if versioncmp($facts['os']['release']['major'],'6') == 0 {
      notify { 'Cannot install logstash on SLC6':}
    }
    elsif versioncmp($facts['os']['release']['major'], '7') >= 0 {

      include dirac::cautils

      # Install logstash repository
      yumrepo { 'logstash-8.x':
          descr    => 'CentOS/RHEL Logstash repository for 8.x packages',
          baseurl  => 'http://linuxsoft.cern.ch/mirror/artifacts.elastic.co/packages/oss-8.x/yum/',
          gpgcheck => 1,
          enabled  => 1,
          priority => 10,
          gpgkey   => 'http://linuxsoft.cern.ch/mirror/artifacts.elastic.co/GPG-KEY-elasticsearch',
          before   => Package['logstash'],
      }

      # Define logstash version and disable ecs compatibility
      # https://forge.puppet.com/modules/puppet/yum/readme#centos-7-and-older

      $logstash_version = '8.13.1'
      if versioncmp($facts['os']['release']['major'],'7') == 0 {
        yum::versionlock{"1:logstash-oss-${logstash_version}-1.x86_64":
          ensure => present,
          before => Package['logstash'],
        }
      }
      elsif versioncmp($facts['os']['release']['major'], '8') >= 0 {
        yum::versionlock{'logstash-oss':
          ensure  => present,
          version => $logstash_version,
          release => '1.*',
          epoch   => 1,
          arch    => 'x86_64',
        }
      }
      -> class { 'logstash':
            version     => $logstash_version,
            settings    => $logstash_settings,
            jvm_options => $logstash_jvm_options,
        }


      # # # Because the low level ruby library used to connect to the message broker alias does not connect to all the host behind the alias
      # # # we need to resolve it ourselves. This is looking for trouble, but well... no choice
      # # # https://github.com/iande/onstomp/issues/31
      # $mb_brokers = unique(query_nodes('hostgroup_0="mig" and hostgroup_1="brokers" and lbaliases="lhcb-test-mb.cern.ch"', 'hostname'))

      # Generate the config file for logstash with Teigi,
      # and then declare it to logstash class
        # a fuller example, including permissions and ownership
      file { '/tmp/logstash/':
        ensure => 'directory',
        owner  => 'logstash',
        mode   => '0750',
      }
      ->teigi::secret::sub_file{'/tmp/logstash/logstashConfigTeigi.cfg':
        teigi_keys => $teigi_keys,
        content    => template($logstash_tpl_path),
        mode       => '0640',
        owner      => 'logstash',
      }
      -> logstash::configfile { 'configname':
        source => '/tmp/logstash/logstashConfigTeigi.cfg',
      }
      -> logstash::plugin { 'logstash-input-stomp': }
      -> logstash::plugin { 'logstash-filter-prune': }
      -> logstash::plugin { ['logstash-output-opensearch']: }
      # Patch the code of the input plugin. The upstream repo seems dead
      # so the likelyhood of a new version or the fix being merged is small
      # https://github.com/logstash-plugins/logstash-input-stomp/issues/23
      -> file_line{'patch logstash connect':
        path  => '/usr/share/logstash/vendor/bundle/jruby/3.1.0/gems/logstash-input-stomp-3.0.8/lib/logstash/inputs/stomp.rb',
        line  => '      connect',
        match => '      self.connect',
      }
      # As of some ruby version, the URI module changed its way to declare new scheme
      # it went from 
      # @@schemes['RSYNC'] = RSYNC
      # to
      # register_scheme 'RSYNC', RSYNC
      # but it does not support protocol with + or . ! 
      # so we delete it
      # https://github.com/ruby/uri/issues/89
      -> file_line{'patch onstomp scheme stomp':
        path  => '/usr/share/logstash/vendor/bundle/jruby/3.1.0/gems/onstomp-1.0.12/lib/onstomp/components/uri.rb',
        match => "  @@schemes\\['STOMP'\\] = OnStomp::Components::URI::STOMP",
        line  => "  register_scheme 'STOMP', OnStomp::Components::URI::STOMP",
      }
      -> file_line{'patch onstomp scheme ssl_stomp':
        ensure            => absent,
        path              => '/usr/share/logstash/vendor/bundle/jruby/3.1.0/gems/onstomp-1.0.12/lib/onstomp/components/uri.rb',
        line              => "  @@schemes['STOMP+SSL'] = OnStomp::Components::URI::STOMP_SSL",
        match             => "  @@schemes\\['STOMP\\+SSL'\\] = OnStomp::Components::URI::STOMP_SSL",
        match_for_absence => true,
      }

      # Proper logrotate configuration to have the logs being rotated
      logrotate::rule {'logstash':
        path         => '/var/log/logstash/*.log',
        rotate       => 10,
        rotate_every => 'week',
        create       => true,
        create_mode  => '0664',
        missingok    => true,
        compress     => true,
      }
    }
}

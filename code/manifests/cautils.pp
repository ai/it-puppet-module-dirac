class dirac::cautils()
{
  notify {'DIRAC CA UTILS':}
  file {'/usr/local/bin/genAllCAs.sh':
    ensure => file,
    source => 'puppet:///modules/dirac/genAllCAs.sh',
    mode   => '0755',
  }  # The cron will run every 2 hours and 10 mn
  -> cron { 'genAllCAsCron':
    command => '/usr/local/bin/genAllCAs.sh &>/dev/null',
    user    => 'root',
    hour    => '*/2',
    minute  => '10',
  }
}
